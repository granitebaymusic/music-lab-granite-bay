We offer drum, guitar, bass guitar, piano, brass, violin, and vocal lessons from some of the very best instructors in the area. We take pride in our wide selection of instructors that include ones that are educated, professionally endorsed, and experienced. They are all unique individuals.

Address: 8745 Auburn Folsom Rd, Granite Bay, CA 95746, USA

Phone: 916-660-6699